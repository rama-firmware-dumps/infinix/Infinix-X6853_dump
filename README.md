## FIRMWARE DUMP
### sys_tssi_64_armv82_infinix-user 14 UP1A.231005.007 774738 release-keys
- Transsion Name: Infinix NOTE 40
- TranOS Build: X6853-H895QSUV-U-OP-241217V3482
- TranOS Version: xos14.0.0
- Brand: INFINIX
- Model: Infinix-X6853
- Platform: mt6789 (Helio G99 Ultimate)
- Android Build: UP1A.231005.007
- Android Version: 14
- Kernel Version: 5.10.209
- Security Patch: 2024-12-05
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Screen Density: 480
- Fingerprint: Infinix/X6853-OP/Infinix-X6853:14/UP1A.231005.007/241217V3482:user/release-keys
